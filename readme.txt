exclude package form testing
==
go test `go list ./... | grep -v YOUR.PACKAGE.NAME`


force cached tests
==
go test -count=1


run one specific test only
==
go test github.com/bla/pkg/YOUR_PACKAGE -run YOUR_TEST_NAME


run test sequentially (one after another)
==
go test -p 1


verbose mode
==
go test -v
